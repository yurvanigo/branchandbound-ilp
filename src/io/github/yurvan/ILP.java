package io.github.yurvan;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ILP {
    private Node max;
    private List<Node> solutionList;

    public ILP(Node max) {
        this.max = max;
        solutionList = new ArrayList<>();
        rootBranch(max);
        System.out.println(max);
    }

    private void rootBranch(Node max){
        Rule a = max.getA();
        Rule b = max.getB();
        Map<String, Double> aEq = solveSimultaneousEquations(
                a.getC1(), a.getC2(), a.getResult(),
                b.getC1(), b.getC2(), b.getResult());
        max.setX1(aEq.get("x"));
        max.setX2(aEq.get("y"));

        Rule z = max.getZ();
        max.getZ().setResult(z.getC1() * max.getX1() + z.getC2() * max.getX2());
        max.getA().setResult(a.getC1() * max.getX1() + a.getC2() * max.getX2());
        max.getB().setResult(b.getC1() * max.getX1() + b.getC2() * max.getX2());
    }

    private void findInteger(Double x1, Double x2){

    }


    // https://stackoverflow.com/a/42280510 <-- inspired by here
    // https://www.intmath.com/matrices-determinants/1-determinants.php <-- logic from here
    public static Map<String, Double> solveSimultaneousEquations(double a1, double b1, double c1, double a2, double b2, double c2){
        double det = ((a1) * (b2) - (a2) * (b1));
        double x = ((c1) * (b2) - (c2) * (b1)) / det;
        double y = ((a1) * (c2) - (a2) * (c1)) / det;
//        System.out.print("x=" + x + " y=" + y);
        DecimalFormat df = new DecimalFormat("#.##");
        Map<String, Double> map = new HashMap<>();
        map.put("x", Double.valueOf(df.format(x)));
        map.put("y", Double.valueOf(df.format(y)));
        return map;
    }

    public static double findX(double a, double b, double c){
        return (c-b)/a;
    }

    public static double findY(double a, double b, double c){
        return (c-a)/b;
    }
}