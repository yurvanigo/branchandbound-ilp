package io.github.yurvan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Node max = new Node.Builder()
                .setZ(new Rule(5,6))
                .setA(new Rule(1, 1, 5))
                .setB(new Rule(4,7,28))
                .build();
        ILP ilp = new ILP(max);
    }

}
