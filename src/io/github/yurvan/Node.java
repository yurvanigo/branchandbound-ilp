package io.github.yurvan;

public class Node {
    private Rule z;
    private Rule a;
    private Rule b;
    private double x1, x2;

    private Node(){ }

    private Node(Rule z, Rule a, Rule b) {
        this.z = z;
        this.a = a;
        this.b = b;
    }

    public Rule getZ() {
        return z;
    }

    public void setZ(Rule z) {
        this.z = z;
    }

    public Rule getA() {
        return a;
    }

    public void setA(Rule a) {
        this.a = a;
    }

    public Rule getB() {
        return b;
    }

    public void setB(Rule b) {
        this.b = b;
    }

    public double getX1() {
        return x1;
    }

    public void setX1(double x1) {
        this.x1 = x1;
    }

    public double getX2() {
        return x2;
    }

    public void setX2(double x2) {
        this.x2 = x2;
    }

    public static class Builder{
        private Rule z;
        private Rule a;
        private Rule b;

        public Builder setZ(Rule z) {
            this.z = z;
            return this;
        }

        public Builder setA(Rule a) {
            this.a = a;
            return this;
        }

        public Builder setB(Rule b) {
            this.b = b;
            return this;
        }

        public Node build(){
            if(z == null || a == null || b == null)
                new Exception("Requirement rules not complete").printStackTrace();
            return new Node(z, a, b);
        }
    }

    @Override
    public String toString() {
        return ("Z = " + z.getC1() + "X1 + " + z.getC2() + "X2, result = " + z.getResult()) +
                "\nA = " + a.getC1() + "X1 + " + a.getC2() + "X2, result = " + a.getResult() +
                "\nB = " + b.getC1() + "X1 + " + b.getC2() + "X2, result = " + b.getResult() +
                "\nX1 = " + x1 + ", X2 = " + x2;
    }
}
