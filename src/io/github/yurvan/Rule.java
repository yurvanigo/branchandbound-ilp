package io.github.yurvan;

import java.text.DecimalFormat;

public class Rule {
    private int c1 = 1, c2 = 1;
    private double result;

    public Rule(int c1, int c2) {
        this.c1 = c1;
        this.c2 = c2;
    }

    public Rule(int c1, int c2, double result) {
        this.c1 = c1;
        this.c2 = c2;
        this.result = result;
    }

    public int getC1() {
        return c1;
    }

    public int getC2() {
        return c2;
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        DecimalFormat df = new DecimalFormat("#.##");
        this.result = Double.parseDouble(df.format(result));
    }
}
